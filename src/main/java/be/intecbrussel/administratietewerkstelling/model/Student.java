package be.intecbrussel.administratietewerkstelling.model;

import be.intecbrussel.administratietewerkstelling.model.studentDocumentFiles.*;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

@Entity
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @NotEmpty
    private String name;
    @OneToOne(orphanRemoval = true)
    private Training training;
    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private StartUp startUp;
    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Ceased ceased;
    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private JobBank jobBank;
    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private InternshipFollowingUp internshipFollowingUp;
    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private ApplicationHours applicationHours;
    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Aftercare aftercare;
    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private StudentProfile studentProfile;

    public Student() {
        startUp = new StartUp();// every student has a start up file
        setStartUp(startUp);
        ceased = new Ceased();
        setCeased(ceased);
        jobBank = new JobBank();
        setJobBank(jobBank);
        internshipFollowingUp = new InternshipFollowingUp();
        setInternshipFollowingUp(internshipFollowingUp);
        applicationHours = new ApplicationHours();
        setApplicationHours(applicationHours);
        aftercare = new Aftercare();
        setAftercare(aftercare);
        studentProfile = new StudentProfile();
        setStudentProfile(studentProfile);

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Training getTraining() {
        return training;
    }

    public void setTraining(Training training) {
        this.training = training;
    }

    public StartUp getStartUp() {
        return startUp;
    }

    public void setStartUp(StartUp startUp) {
        this.startUp = startUp;
        startUp.setStudent(this);
    }

    public Ceased getCeased() {
        return ceased;
    }

    public void setCeased(Ceased ceased) {
        this.ceased = ceased;
        ceased.setStudent(this);
    }

    public JobBank getJobBank() {
        return jobBank;
    }

    public void setJobBank(JobBank jobBank) {
        this.jobBank = jobBank;
        jobBank.setStudent(this);
    }

    public InternshipFollowingUp getInternshipFollowingUp() {
        return internshipFollowingUp;
    }

    public void setInternshipFollowingUp(InternshipFollowingUp internshipFollowingUp) {
        this.internshipFollowingUp = internshipFollowingUp;
    }

    public ApplicationHours getApplicationHours() {
        return applicationHours;
    }

    public void setApplicationHours(ApplicationHours applicationHours) {
        this.applicationHours = applicationHours;
        applicationHours.setStudent(this);
    }

    public Aftercare getAftercare() {
        return aftercare;
    }

    public void setAftercare(Aftercare aftercare) {
        this.aftercare = aftercare;
        aftercare.setStudent(this);
    }

    public StudentProfile getStudentProfile() {
        return studentProfile;
    }

    public void setStudentProfile(StudentProfile studentProfile) {
        this.studentProfile = studentProfile;
        studentProfile.setStudent(this);
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", training=" + training +
                ", startUp=" + startUp +
                ", ceased=" + ceased +
                ", jobBank=" + jobBank +
                ", internshipFollowingUp=" + internshipFollowingUp +
                ", applicationHours=" + applicationHours +
                ", aftercare=" + aftercare +
                ", studentProfile=" + studentProfile +
                '}';
    }
}
