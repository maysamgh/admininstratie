package be.intecbrussel.administratietewerkstelling.model.enums;

public enum Reason {
    TECH_ONGESGHIKT, WERK, DOOR_KLANT, ZIEKTE, ORDE_TOCH, STAGE
}
