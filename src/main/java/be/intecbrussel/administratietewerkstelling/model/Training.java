package be.intecbrussel.administratietewerkstelling.model;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Training {

    //31 properties!! check!!!

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @NotEmpty
    private String name;

    @OneToMany(mappedBy = "training", cascade = {CascadeType.ALL})
    private List<Student> students = new ArrayList<>();

    private LocalDate startDate;
    private LocalDate endDate;
    private LocalDate the20Applications;
    private String link;
    private int ceased;
    private int internship;
    private int job;
    @ManyToMany(mappedBy = "trainings", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Company> companies = new ArrayList<>(); //???????????????????
    private boolean overviewVDABInOrder;
    private boolean overviewVDABCheck;
    private boolean wallPictureInOrder;
    private boolean wallPictureCheck;
    private boolean planningInOrder;
    private boolean planningCheck;
    private boolean overviewPictureMapInorder;
    private boolean overviewPictureMapCheck;
    private boolean redThreadInOrder;
    private boolean redThreadCheck;
    private boolean scoreFormInOrder;
    private boolean scoreFormCheck;
    private boolean digitalFileInOrder;
    private boolean digitalFileCheck;
    private boolean fileAndBoxInOrder;
    private boolean fileAndBoxCheck;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
//        if(startDate.isBefore(endDate)){
//            this.startDate = startDate;
//        }
//        else if (endDate.equals(null)){
//            this.startDate = startDate;
//        }
//        else{
//           // throw conflictException make one
//        }

    }

    public List<Company> getCompanies() {
        return companies;
    }

    public void addCompany(Company company) {
        companies.add(company);
    }

    public void setCompanies(List<Company> companies) {
        this.companies = companies;
    }

    public boolean isOverviewVDABInOrder() {
        return overviewVDABInOrder;
    }

    public void setOverviewVDABInOrder(boolean overviewVDABInOrder) {
        this.overviewVDABInOrder = overviewVDABInOrder;
    }

    public boolean isOverviewVDABCheck() {
        return overviewVDABCheck;
    }

    public void setOverviewVDABCheck(boolean overviewVDABCheck) {
        this.overviewVDABCheck = overviewVDABCheck;
    }

    public boolean isWallPictureInOrder() {
        return wallPictureInOrder;
    }

    public void setWallPictureInOrder(boolean wallPictureInOrder) {
        this.wallPictureInOrder = wallPictureInOrder;
    }

    public boolean isWallPictureCheck() {
        return wallPictureCheck;
    }

    public void setWallPictureCheck(boolean wallPictureCheck) {
        this.wallPictureCheck = wallPictureCheck;
    }

    public boolean isPlanningInOrder() {
        return planningInOrder;
    }

    public void setPlanningInOrder(boolean planningInOrder) {
        this.planningInOrder = planningInOrder;
    }

    public boolean isPlanningCheck() {
        return planningCheck;
    }

    public void setPlanningCheck(boolean planningCheck) {
        this.planningCheck = planningCheck;
    }

    public boolean isOverviewPictureMapInorder() {
        return overviewPictureMapInorder;
    }

    public void setOverviewPictureMapInorder(boolean overviewPictureMapInorder) {
        this.overviewPictureMapInorder = overviewPictureMapInorder;
    }

    public boolean isOverviewPictureMapCheck() {
        return overviewPictureMapCheck;
    }

    public void setOverviewPictureMapCheck(boolean overviewPictureMapCheck) {
        this.overviewPictureMapCheck = overviewPictureMapCheck;
    }

    public boolean isRedThreadInOrder() {
        return redThreadInOrder;
    }

    public void setRedThreadInOrder(boolean redThreadInOrder) {
        this.redThreadInOrder = redThreadInOrder;
    }

    public boolean isRedThreadCheck() {
        return redThreadCheck;
    }

    public void setRedThreadCheck(boolean redThreadCheck) {
        this.redThreadCheck = redThreadCheck;
    }

    public boolean isScoreFormInOrder() {
        return scoreFormInOrder;
    }

    public void setScoreFormInOrder(boolean scoreFormInOrder) {
        this.scoreFormInOrder = scoreFormInOrder;
    }

    public boolean isScoreFormCheck() {
        return scoreFormCheck;
    }

    public void setScoreFormCheck(boolean scoreFormCheck) {
        this.scoreFormCheck = scoreFormCheck;
    }

    public boolean isDigitalFileInOrder() {
        return digitalFileInOrder;
    }

    public void setDigitalFileInOrder(boolean digitalFileInOrder) {
        this.digitalFileInOrder = digitalFileInOrder;
    }

    public boolean isDigitalFileCheck() {
        return digitalFileCheck;
    }

    public void setDigitalFileCheck(boolean digitalFileCheck) {
        this.digitalFileCheck = digitalFileCheck;
    }

    public boolean isFileAndBoxInOrder() {
        return fileAndBoxInOrder;
    }

    public void setFileAndBoxInOrder(boolean fileAndBoxInOrder) {
        this.fileAndBoxInOrder = fileAndBoxInOrder;
    }

    public boolean isFileAndBoxCheck() {
        return fileAndBoxCheck;
    }

    public void setFileAndBoxCheck(boolean fileAndBoxCheck) {
        this.fileAndBoxCheck = fileAndBoxCheck;
    }

    public LocalDate get80ProcentTraining() {
        long days = (ChronoUnit.DAYS.between(startDate, endDate)) * 80 / 100;
        return startDate.plusDays(days);
    }

    public int getQuantityInTraining() {
        return students.size() - (internship + ceased + job);
    }


    public void addStudent(Student student) {
        students.add(student);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }


    public void setStartDatum(LocalDate startDatum) {
        this.startDate = startDatum;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public LocalDate getThe20Applications() {
        return the20Applications;
    }

    public void setThe20Applications(LocalDate the20Applications) {
        this.the20Applications = the20Applications;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public int getCeased() {
        return ceased;
    }

    public void setCeased(int ceased) {
        this.ceased = ceased;
    }

    public int getInternship() {
        return internship;
    }

    public void setInternship(int internship) {
        this.internship = internship;
    }

    public int getJob() {
        return job;
    }

    public void setJob(int job) {
        this.job = job;
    }

    @Override
    public String toString() {
        return "Training{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
