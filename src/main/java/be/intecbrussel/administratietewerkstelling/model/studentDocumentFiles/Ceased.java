package be.intecbrussel.administratietewerkstelling.model.studentDocumentFiles;

import be.intecbrussel.administratietewerkstelling.model.Student;
import be.intecbrussel.administratietewerkstelling.model.enums.Reason;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
public class Ceased {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @OneToOne
    private Student student;
    private LocalDate lastDay;
    private Reason reason;
    private LocalDate endAftercare;
    private boolean contract;
    private boolean AEO;
    private boolean WO;
    private boolean VDABFile;
    private boolean certificate;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public LocalDate getLastDay() {
        return lastDay;
    }

    public void setLastDay(LocalDate lastDay) {
        this.lastDay = lastDay;
    }

    public Reason getReason() {
        return reason;
    }

    public void setReason(Reason reason) {
        this.reason = reason;
    }

    public LocalDate getEndAftercare() {
        return endAftercare;
    }

    public void setEndAftercare(LocalDate endAftercare) {
        this.endAftercare = endAftercare;
    }

    public boolean isContract() {
        return contract;
    }

    public void setContract(boolean contract) {
        this.contract = contract;
    }

    public boolean isAEO() {
        return AEO;
    }

    public void setAEO(boolean AEO) {
        this.AEO = AEO;
    }

    public boolean isWO() {
        return WO;
    }

    public void setWO(boolean WO) {
        this.WO = WO;
    }

    public boolean isVDABFile() {
        return VDABFile;
    }

    public void setVDABFile(boolean VDABFile) {
        this.VDABFile = VDABFile;
    }

    public boolean isCertificate() {
        return certificate;
    }

    public void setCertificate(boolean certificate) {
        this.certificate = certificate;
    }
}
