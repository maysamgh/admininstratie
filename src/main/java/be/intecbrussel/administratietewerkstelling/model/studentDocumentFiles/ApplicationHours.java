package be.intecbrussel.administratietewerkstelling.model.studentDocumentFiles;

import be.intecbrussel.administratietewerkstelling.model.Student;

import javax.persistence.*;

@Entity
public class ApplicationHours {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @OneToOne
    private Student student;
    private boolean the20ApplicationsDeadline;
    private boolean firstApplicationHour;
    private boolean secondApplicationHour;
    private boolean thirdApplicationHour;
    private boolean fourthApplicationHour;
    private boolean fifthApplicationHour;
    private boolean endTrainingReason;
    private boolean lastDayInTraining;

    public ApplicationHours() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public boolean isThe20ApplicationsDeadline() {
        return the20ApplicationsDeadline;
    }

    public void setThe20ApplicationsDeadline(boolean the20ApplicationsDeadline) {
        this.the20ApplicationsDeadline = the20ApplicationsDeadline;
    }

    public boolean isFirstApplicationHour() {
        return firstApplicationHour;
    }

    public void setFirstApplicationHour(boolean firstApplicationHour) {
        this.firstApplicationHour = firstApplicationHour;
    }

    public boolean isSecondApplicationHour() {
        return secondApplicationHour;
    }

    public void setSecondApplicationHour(boolean secondApplicationHour) {
        this.secondApplicationHour = secondApplicationHour;
    }

    public boolean isThirdApplicationHour() {
        return thirdApplicationHour;
    }

    public void setThirdApplicationHour(boolean thirdApplicationHour) {
        this.thirdApplicationHour = thirdApplicationHour;
    }

    public boolean isFourthApplicationHour() {
        return fourthApplicationHour;
    }

    public void setFourthApplicationHour(boolean fourthApplicationHour) {
        this.fourthApplicationHour = fourthApplicationHour;
    }

    public boolean isFifthApplicationHour() {
        return fifthApplicationHour;
    }

    public void setFifthApplicationHour(boolean fifthApplicationHour) {
        this.fifthApplicationHour = fifthApplicationHour;
    }

    public boolean isEndTrainingReason() {
        return endTrainingReason;
    }

    public void setEndTrainingReason(boolean endTrainingReason) {
        this.endTrainingReason = endTrainingReason;
    }

    public boolean isLastDayInTraining() {
        return lastDayInTraining;
    }

    public void setLastDayInTraining(boolean lastDayInTraining) {
        this.lastDayInTraining = lastDayInTraining;
    }
}
