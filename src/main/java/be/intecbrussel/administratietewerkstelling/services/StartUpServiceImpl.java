package be.intecbrussel.administratietewerkstelling.services;

import be.intecbrussel.administratietewerkstelling.model.studentDocumentFiles.StartUp;
import be.intecbrussel.administratietewerkstelling.repositories.StartUpRepository;
import be.intecbrussel.administratietewerkstelling.services.interfaces.StartUpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class StartUpServiceImpl implements StartUpService {
    @Autowired
    StartUpRepository startUpRepository;

    public Optional<StartUp> getStartUpById(int id) {
        return startUpRepository.findById(id);
    }

    @Override
    public ResponseEntity<StartUp> updateStartUp(StartUp startUp) {
        if (startUpRepository.findById(startUp.getId()).isPresent()) {
            startUpRepository.findById(startUp.getId()).get();
            startUp.setIWW(startUp.isIWW());
            startUp.setJobProfile(startUp.isJobProfile());
            startUp.setBeginTPR(startUp.isBeginTPR());
            startUp.setSBTAndOverview(startUp.isSBTAndOverview());
            startUp.setGuidanceProgram(startUp.isGuidanceProgram());
            startUp.setUploadMLP(startUp.isUploadMLP());
            startUp.setVDABFile(startUp.isVDABFile());

            return new ResponseEntity<>(startUpRepository.save(startUp), HttpStatus.OK);
        } else return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}
