package be.intecbrussel.administratietewerkstelling.services;

import be.intecbrussel.administratietewerkstelling.model.studentDocumentFiles.Ceased;
import be.intecbrussel.administratietewerkstelling.repositories.CeasedRepository;
import be.intecbrussel.administratietewerkstelling.services.interfaces.CeasedService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CeasedServiceImpl implements CeasedService {
    @Autowired
    CeasedRepository ceasedRepository;

    public ResponseEntity<Ceased> updateCeased(Ceased ceased) {
        if (ceasedRepository.findById(ceased.getId()).isPresent()) {
            ceasedRepository.findById(ceased.getId()).get();
            ceased.setLastDay(ceased.getLastDay());
            ceased.setReason(ceased.getReason());
            ceased.setEndAftercare(ceased.getEndAftercare());
            ceased.setContract(ceased.isContract());
            ceased.setAEO(ceased.isAEO());
            ceased.setWO(ceased.isWO());
            ceased.setVDABFile(ceased.isVDABFile());
            ceased.setCertificate(ceased.isCertificate());

            return new ResponseEntity<>(ceasedRepository.save(ceased), HttpStatus.OK);
        } else return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    public Optional<Ceased> getCeasedById(int id) { //TODO why optional ceased?? kan vervangen door if clause and exception?
        return ceasedRepository.findById(id);
    }
}
