package be.intecbrussel.administratietewerkstelling.services.interfaces;

import be.intecbrussel.administratietewerkstelling.model.studentDocumentFiles.Aftercare;
import org.springframework.http.ResponseEntity;

public interface AftercareService {
    ResponseEntity<Aftercare> updateAftercare(Aftercare aftercare);
}
