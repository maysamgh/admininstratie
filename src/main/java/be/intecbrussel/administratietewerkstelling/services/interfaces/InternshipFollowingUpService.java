package be.intecbrussel.administratietewerkstelling.services.interfaces;

import be.intecbrussel.administratietewerkstelling.model.studentDocumentFiles.InternshipFollowingUp;
import org.springframework.http.ResponseEntity;

public interface InternshipFollowingUpService {
    ResponseEntity<InternshipFollowingUp> updateInternshipFollowingUp(InternshipFollowingUp internshipFollowingUp);
}
