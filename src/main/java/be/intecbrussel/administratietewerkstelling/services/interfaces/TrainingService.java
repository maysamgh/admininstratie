package be.intecbrussel.administratietewerkstelling.services.interfaces;

import be.intecbrussel.administratietewerkstelling.model.Company;
import be.intecbrussel.administratietewerkstelling.model.Training;

import java.util.List;

public interface TrainingService {
    Training saveTraining(Training training);

    void deleteTrainingById(int id);

    Training updateTraining(Training training);

    Training getTrainingById(int id);

    List<Training> findAllTrainings();

    void addCompany(Training training, Company company);
}
