package be.intecbrussel.administratietewerkstelling.services.interfaces;

import be.intecbrussel.administratietewerkstelling.model.Student;
import be.intecbrussel.administratietewerkstelling.model.Training;

import java.util.List;
import java.util.Optional;

public interface StudentService {
    Student saveStudent(Training training, Student student);

    List<Student> findAllStudents();

    Optional<Student> getStudentById(int id);
}
