package be.intecbrussel.administratietewerkstelling.services.interfaces;

import be.intecbrussel.administratietewerkstelling.model.studentDocumentFiles.JobBank;
import org.springframework.http.ResponseEntity;

public interface JobBankService {
    ResponseEntity<JobBank> updateJobBank(JobBank jobBank);
}
