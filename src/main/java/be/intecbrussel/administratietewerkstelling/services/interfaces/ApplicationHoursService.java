package be.intecbrussel.administratietewerkstelling.services.interfaces;

import be.intecbrussel.administratietewerkstelling.model.studentDocumentFiles.ApplicationHours;
import org.springframework.http.ResponseEntity;

public interface ApplicationHoursService {
    ResponseEntity<ApplicationHours> updateApplicationHours(ApplicationHours applicationHours);
}
