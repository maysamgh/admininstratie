package be.intecbrussel.administratietewerkstelling.services.interfaces;

import be.intecbrussel.administratietewerkstelling.model.studentDocumentFiles.StudentProfile;
import org.springframework.http.ResponseEntity;

public interface StudentProfileService {
    ResponseEntity<StudentProfile> updateStudentProfile(StudentProfile studentProfile);
}
