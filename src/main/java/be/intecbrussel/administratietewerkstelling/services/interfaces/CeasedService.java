package be.intecbrussel.administratietewerkstelling.services.interfaces;

import be.intecbrussel.administratietewerkstelling.model.studentDocumentFiles.Ceased;
import org.springframework.http.ResponseEntity;

public interface CeasedService {
    ResponseEntity<Ceased> updateCeased(Ceased ceased);
}
