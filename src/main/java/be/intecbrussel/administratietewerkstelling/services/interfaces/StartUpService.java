package be.intecbrussel.administratietewerkstelling.services.interfaces;

import be.intecbrussel.administratietewerkstelling.model.studentDocumentFiles.StartUp;
import org.springframework.http.ResponseEntity;

public interface StartUpService {
    ResponseEntity<StartUp> updateStartUp(StartUp startUp);
}
