package be.intecbrussel.administratietewerkstelling.services;

import be.intecbrussel.administratietewerkstelling.model.Company;
import be.intecbrussel.administratietewerkstelling.model.Student;
import be.intecbrussel.administratietewerkstelling.model.Training;
import be.intecbrussel.administratietewerkstelling.repositories.TrainingRepository;
import be.intecbrussel.administratietewerkstelling.services.interfaces.TrainingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TrainingServiceImpl implements TrainingService {
    @Autowired
    private TrainingRepository trainingRepository;

    public Training saveTraining(Training training) {
        return !trainingRepository.existsById(training.getId()) ? trainingRepository.save(training) : null;
    }

    public void deleteTrainingById(int id) {
        trainingRepository.deleteById(id);
    }

    public Training updateTraining(Training training) {
        training.setCeased(training.getCeased());

        training.setThe20Applications(training.getThe20Applications());
        training.setLink(training.getLink());
        training.setJob(training.getJob());
        training.setStartDate(training.getStartDate());
        training.setEndDate(training.getEndDate());

        training.setDigitalFileCheck(training.isDigitalFileCheck());
        training.setDigitalFileInOrder(training.isDigitalFileInOrder());

        training.setOverviewVDABCheck(training.isOverviewVDABCheck());
        training.setOverviewVDABInOrder(training.isOverviewVDABInOrder());

        training.setWallPictureCheck(training.isWallPictureCheck());
        training.setWallPictureInOrder(training.isWallPictureInOrder());

        training.setPlanningCheck(training.isPlanningCheck());
        training.setPlanningInOrder(training.isPlanningInOrder());

        training.setOverviewPictureMapCheck(training.isOverviewPictureMapCheck());
        training.setOverviewVDABInOrder(training.isOverviewPictureMapInorder());

        training.setRedThreadCheck(training.isRedThreadCheck());
        training.setRedThreadInOrder(training.isRedThreadInOrder());

        training.setScoreFormCheck(training.isScoreFormCheck());
        training.setScoreFormInOrder(training.isScoreFormInOrder());

        training.setFileAndBoxCheck(training.isFileAndBoxCheck());
        training.setFileAndBoxInOrder(training.isFileAndBoxInOrder());

        return training;

    }

    @Override
    public void addCompany(Training training, Company company) {
        training.addCompany(company);
    }

    public Training getTrainingById(int id) {
        return trainingRepository.findTrainingById(id);
    }

    public List<Training> findAllTrainings() {
        return trainingRepository.findAll();
    }

    public List<Student> getStudentsByTraining(Training training) {
        return training.getStudents();
    }

}
