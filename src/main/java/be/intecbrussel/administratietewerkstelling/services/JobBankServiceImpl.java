package be.intecbrussel.administratietewerkstelling.services;

import be.intecbrussel.administratietewerkstelling.model.studentDocumentFiles.JobBank;
import be.intecbrussel.administratietewerkstelling.repositories.JobBankRepository;
import be.intecbrussel.administratietewerkstelling.services.interfaces.JobBankService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class JobBankServiceImpl implements JobBankService {
    @Autowired
    JobBankRepository jobBankRepository;

    @Override
    public ResponseEntity<JobBank> updateJobBank(JobBank jobBank) {
        if (jobBankRepository.findById(jobBank.getId()).isPresent()) {
            jobBankRepository.findById(jobBank.getId()).get();
            jobBank.setSubmittedCV(jobBank.isSubmittedCV());
            jobBank.setFirstVersionCV(jobBank.isFirstVersionCV());
            jobBank.setSecondVersionCV(jobBank.isSecondVersionCV());
            jobBank.setSubmitMotivation(jobBank.isSubmitMotivation());
            jobBank.setFirstVersionMotivation(jobBank.isFirstVersionMotivation());
            jobBank.setSecondVersionMotivation(jobBank.isSecondVersionMotivation());
            jobBank.setFirstExerciseConversationFeedback(jobBank.isFirstExerciseConversationFeedback());
            jobBank.setFirstLinkedinFeedback(jobBank.isFirstLinkedinFeedback());
            jobBank.setSecondVersionMotivation(jobBank.isSecondLinkedinFeedback());

            return new ResponseEntity<>(jobBankRepository.save(jobBank), HttpStatus.OK);
        } else return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    public Optional<JobBank> getJobBankById(int id) {
        return jobBankRepository.findById(id);
    }
}
