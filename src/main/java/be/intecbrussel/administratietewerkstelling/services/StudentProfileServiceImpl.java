package be.intecbrussel.administratietewerkstelling.services;

import be.intecbrussel.administratietewerkstelling.model.studentDocumentFiles.StudentProfile;
import be.intecbrussel.administratietewerkstelling.repositories.StudentProfileRepository;
import be.intecbrussel.administratietewerkstelling.services.interfaces.StudentProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class StudentProfileServiceImpl implements StudentProfileService {
    @Autowired
    StudentProfileRepository studentProfileRepository;

    @Override
    public ResponseEntity<StudentProfile> updateStudentProfile(StudentProfile studentProfile) {
        if (studentProfileRepository.findById(studentProfile.getId()).isPresent()) {
            studentProfileRepository.findById(studentProfile.getId()).get();
            studentProfile.setAddress(studentProfile.getAddress());
            studentProfile.setDriversLicense(studentProfile.getDriversLicense());
            studentProfile.setLanguageSkills(studentProfile.getLanguageSkills());
            studentProfile.setPhoneNumber(studentProfile.getPhoneNumber());
            studentProfile.setEmail(studentProfile.getEmail());
            studentProfile.setItExperience(studentProfile.getItExperience());
            studentProfile.setWorkRegion(studentProfile.getWorkRegion());
            studentProfile.setNotes(studentProfile.getNotes());
            studentProfile.setContactMoments(studentProfile.getContactMoments());
            studentProfile.setPermissionCVToCompanies(studentProfile.getPermissionCVToCompanies());

            return new ResponseEntity<>(studentProfileRepository.save(studentProfile), HttpStatus.OK);
        } else return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    public Optional<StudentProfile> getStudentProfileById(int id) {
        return studentProfileRepository.findById(id);
    }
}
