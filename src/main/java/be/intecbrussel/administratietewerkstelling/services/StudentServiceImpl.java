package be.intecbrussel.administratietewerkstelling.services;

import be.intecbrussel.administratietewerkstelling.model.Student;
import be.intecbrussel.administratietewerkstelling.model.Training;
import be.intecbrussel.administratietewerkstelling.repositories.StudentRepository;
import be.intecbrussel.administratietewerkstelling.services.interfaces.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class StudentServiceImpl implements StudentService {
    @Autowired
    private StudentRepository studentRepository;

    public Student saveStudent(Training training, Student student) {
        if (!studentRepository.existsById(student.getId())) {
            training.addStudent(student);
            student.setTraining(training);
            studentRepository.save(student);
            return student;
        } else
            return null;
    }

    public List<Student> findAllStudents() {
        return studentRepository.findAll();
    }

    public List<Student> findStudendsByTraining(Training training) {
        return studentRepository.findStudentsByTraining(training);
    }

    public Optional<Student> getStudentById(int id) {
        return studentRepository.findById(id);

    }


}

