package be.intecbrussel.administratietewerkstelling.services;

import be.intecbrussel.administratietewerkstelling.model.studentDocumentFiles.ApplicationHours;
import be.intecbrussel.administratietewerkstelling.repositories.ApplicationHoursRepository;
import be.intecbrussel.administratietewerkstelling.services.interfaces.ApplicationHoursService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ApplicationHoursServiceImpl implements ApplicationHoursService {
    @Autowired
    ApplicationHoursRepository applicationHoursRepository;

    @Override
    public ResponseEntity<ApplicationHours> updateApplicationHours(ApplicationHours applicationHours) {
        if (applicationHoursRepository.findById(applicationHours.getId()).isPresent()) {
            applicationHoursRepository.findById(applicationHours.getId()).get();

            applicationHours.setThe20ApplicationsDeadline(applicationHours.isThe20ApplicationsDeadline());
            applicationHours.setFifthApplicationHour(applicationHours.isFirstApplicationHour());
            applicationHours.setSecondApplicationHour(applicationHours.isSecondApplicationHour());
            applicationHours.setThirdApplicationHour(applicationHours.isThirdApplicationHour());
            applicationHours.setFourthApplicationHour(applicationHours.isFourthApplicationHour());
            applicationHours.setFifthApplicationHour(applicationHours.isFifthApplicationHour());
            applicationHours.setEndTrainingReason(applicationHours.isEndTrainingReason());
            applicationHours.setLastDayInTraining(applicationHours.isLastDayInTraining());

            return new ResponseEntity<>(applicationHoursRepository.save(applicationHours), HttpStatus.OK);
        } else return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    public Optional<ApplicationHours> getApplicationHoursById(int id) {
        return applicationHoursRepository.findById(id);
    }
}
