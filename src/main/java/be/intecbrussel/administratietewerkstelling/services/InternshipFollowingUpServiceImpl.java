package be.intecbrussel.administratietewerkstelling.services;

import be.intecbrussel.administratietewerkstelling.model.studentDocumentFiles.InternshipFollowingUp;
import be.intecbrussel.administratietewerkstelling.repositories.InternshipFollowingUpRepository;
import be.intecbrussel.administratietewerkstelling.services.interfaces.InternshipFollowingUpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class InternshipFollowingUpServiceImpl implements InternshipFollowingUpService {
    @Autowired
    InternshipFollowingUpRepository internshipFollowingUpRepository;

    @Override
    public ResponseEntity<InternshipFollowingUp> updateInternshipFollowingUp(InternshipFollowingUp internshipFollowingUp) {

        if (internshipFollowingUpRepository.findById(internshipFollowingUp.getId()).isPresent()) {
            internshipFollowingUpRepository.findById(internshipFollowingUp.getId()).get();
            internshipFollowingUp.setInternshipMessage(internshipFollowingUp.isInternshipMessage());
            internshipFollowingUp.setFirstDay(internshipFollowingUp.getFirstDay());
            internshipFollowingUp.setLastDay(internshipFollowingUp.getLastDay());
            internshipFollowingUp.setWeek1(internshipFollowingUp.isWeek1());
            internshipFollowingUp.setWeek2(internshipFollowingUp.isWeek2());
            internshipFollowingUp.setWeek3(internshipFollowingUp.isWeek3());
            internshipFollowingUp.setWeek4(internshipFollowingUp.isWeek4());
            internshipFollowingUp.setWeek5(internshipFollowingUp.isWeek5());
            internshipFollowingUp.setWeek6(internshipFollowingUp.isWeek6());
            internshipFollowingUp.setInternshipEvaluation(internshipFollowingUp.isInternshipEvaluation());

            return new ResponseEntity<>(internshipFollowingUpRepository.save(internshipFollowingUp), HttpStatus.OK);
        } else return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    public Optional<InternshipFollowingUp> getInternshipFollowingUpById(int id) {
        return internshipFollowingUpRepository.findById(id);
    }
}

