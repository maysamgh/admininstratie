package be.intecbrussel.administratietewerkstelling.controllers;

import be.intecbrussel.administratietewerkstelling.model.studentDocumentFiles.Aftercare;
import be.intecbrussel.administratietewerkstelling.services.AftercareServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PutMapping;

import java.util.Optional;

//TODO : Discuss difference between @ModelAttribute vs @Model model, add attribute
//
@Controller
public class AftercareController {

    @Autowired
    AftercareServiceImpl aftercareServiceInterface;

    @PutMapping()
    public String updateAftercare(@ModelAttribute Aftercare aftercare) {
        aftercareServiceInterface.updateAftercare(aftercare);
        return "redirect:/student/" + aftercare.getStudent().getId();

    }

    @GetMapping()
    public String getAftercare(@ModelAttribute int id, Model model) {
        Optional<Aftercare> aftercare = aftercareServiceInterface.getAftercareById(id);
        if (aftercare.isPresent()) {
            model.addAttribute("aftercare", aftercare);
            return "redirect:/";
        } else return "errorPage";
    }
}
