package be.intecbrussel.administratietewerkstelling.controllers;

import be.intecbrussel.administratietewerkstelling.model.Student;
import be.intecbrussel.administratietewerkstelling.services.StudentServiceImpl;
import be.intecbrussel.administratietewerkstelling.services.TrainingServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.Optional;

@Controller
//@RequestMapping("/student") //this should happen on index page!!!!
public class StudentController {
    @Autowired
    StudentServiceImpl studentService;

    @Autowired
    TrainingServiceImpl trainingService;


    @PostMapping()

    public String saveStudent(@ModelAttribute Student student) {

        System.out.println(student);
        studentService.saveStudent(student.getTraining(), student);
        return "redirect:/trainings/" + student.getTraining().getId();
        /*
         Training training=trainingService.getTrainingById(id);
        studentService.saveStudent(training, student);
        //return "redirect:"; //get method voor student mapping //redirect:/nieuw mapping
        return "redirect:/trainings/"+id;
         */

    }

    @GetMapping("student/{id}")
    public String getStudent(@PathVariable int id, Model model) {
        Optional<Student> student = studentService.getStudentById(id);
        if (student.isPresent()) {
            model.addAttribute("student", student);
            model.addAttribute("aftercare", student.get().getAftercare());
            model.addAttribute("applicationHours", student.get().getApplicationHours());
            model.addAttribute("ceased", student.get().getCeased());
            model.addAttribute("jobBank", student.get().getJobBank());
            model.addAttribute("startUp", student.get().getStartUp());
            model.addAttribute("studentProfile", student.get().getStudentProfile());
            return "student";
        } else
            return "errorPage";
    }
}
