package be.intecbrussel.administratietewerkstelling.controllers;

import be.intecbrussel.administratietewerkstelling.model.studentDocumentFiles.Ceased;
import be.intecbrussel.administratietewerkstelling.services.CeasedServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PutMapping;


//TODO: annotation needed to add path variables here ( or enough in methods? )

//Invalid mapping on handler class
@Controller
public class CeasedController {

    @Autowired
    CeasedServiceImpl ceasedInterfaceImpl;

//    @GetMapping("/gestoptDocument/${id}")
//    public String getCeased(@PathVariable int id, Model model) {
//        Optional<Ceased> ceased = ceasedInterfaceImpl.getCeasedById(id); //TODO research Optional
//        if (ceased.isPresent()) {
//            model.addAttribute("ceased", ceased);
//            return "ceasedDocument/"; //+ ceased.getId(); //TODO fix the getId method!!!
//        } else return "errorCeased";
//    }

    @PutMapping("/gestoptDocument")
    public String updateApplicationHours(@ModelAttribute Ceased ceased) {
        ceasedInterfaceImpl.updateCeased(ceased);
        return "redirect:/student/" + ceased.getStudent().getId();

    }

}


