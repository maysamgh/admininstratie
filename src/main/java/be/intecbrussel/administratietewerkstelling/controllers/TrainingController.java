package be.intecbrussel.administratietewerkstelling.controllers;

import be.intecbrussel.administratietewerkstelling.model.Company;
import be.intecbrussel.administratietewerkstelling.model.Student;
import be.intecbrussel.administratietewerkstelling.model.Training;
import be.intecbrussel.administratietewerkstelling.services.TrainingServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

//mappings moeten in het nederlands, trainings is dus opleidingen ( om het makkelijker te maken voor Cindy )
@Controller
public class TrainingController {
    @Autowired
    TrainingServiceImpl trainingService;

    @GetMapping("/trainings")
    public String findAllTrainings(Model model) {
        List<Training> trainings = trainingService.findAllTrainings();
        model.addAttribute("trainings", trainings);
        return "indexMaysam";
    }

    @GetMapping("/trainings/{id}")
    public String getTrainingById(@PathVariable int id, Model model) {
        Training training = trainingService.getTrainingById(id);
        List<Student> students = trainingService.getStudentsByTraining(training);
        model.addAttribute("training", training);
        model.addAttribute("students", students);
        Student student = new Student();
        student.setTraining(training);
        model.addAttribute("student", student);
        return "training"; //omdat students list onderaan bij training staat
    }

    @PutMapping("/bewerken")
    public String updateTraining(@ModelAttribute Training training, Model model) {
        trainingService.updateTraining(training);
        return "redirect:/trainings";

    }

    @PutMapping("/company")
    public String addCompanyToTraining(@ModelAttribute Training training, @RequestBody Company company) {
        trainingService.addCompany(training, company);
        return "redirect:/trainings";
    }

    @PostMapping("/new")
    public String saveTraining(@ModelAttribute Training training, Model model) {
        trainingService.saveTraining(training);
        model.addAttribute("training", training);
        return "redirect:/trainings";
    }

    @DeleteMapping("/delete/{id}")
    public String deleteTraining(@PathVariable int id, Model model) {
        trainingService.deleteTrainingById(id);
        return "redirect:/trainings";
    }
}
