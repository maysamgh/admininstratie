package be.intecbrussel.administratietewerkstelling.controllers;

import be.intecbrussel.administratietewerkstelling.model.studentDocumentFiles.StartUp;
import be.intecbrussel.administratietewerkstelling.services.StartUpServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Optional;

@Controller
//@RequestMapping("/startUpDocument/${id}")
public class StartUpController {
    @Autowired
    StartUpServiceImpl startUpService;

    @GetMapping("/student/startup")
    public String getStartUpById(@PathVariable int id, Model model) {
        Optional<StartUp> startUp = startUpService.getStartUpById(id);
        if (startUp.isPresent()) {
            model.addAttribute("startUp", startUp);
            return "startUp";
        } else return "errorJobBank";
    }
}
