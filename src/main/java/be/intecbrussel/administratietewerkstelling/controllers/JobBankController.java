package be.intecbrussel.administratietewerkstelling.controllers;

import be.intecbrussel.administratietewerkstelling.model.studentDocumentFiles.JobBank;
import be.intecbrussel.administratietewerkstelling.services.JobBankServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Optional;

@Controller
//@RequestMapping("/werkbankDocument/${id}")
public class JobBankController {

    @Autowired
    JobBankServiceImpl jobBankServiceImpl;

    @GetMapping("/student")
    public String getJobBankById(@PathVariable int id, Model model) {
        Optional<JobBank> jobBank = jobBankServiceImpl.getJobBankById(id);
        if (jobBank.isPresent()) {
            model.addAttribute("jobBank", jobBank);
            return "jobBank";
        } else return "errorJobBank";
    }
}
