package be.intecbrussel.administratietewerkstelling.controllers;

import be.intecbrussel.administratietewerkstelling.model.studentDocumentFiles.StudentProfile;
import be.intecbrussel.administratietewerkstelling.services.StudentProfileServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Optional;

/**
 * sjkfldk
 */

//TODO:


@Controller
//@RequestMapping("/studentProfielDocument/${id}")
public class StudentProfileControler {
    @Autowired
    StudentProfileServiceImpl studentProfileServiceImpl;

    @GetMapping("studentprofile")
    public String getInternshipFollowingUpById(@PathVariable int id, Model model) {
        Optional<StudentProfile> studentProfile = studentProfileServiceImpl.getStudentProfileById(id);
        if (studentProfile.isPresent()) {
            model.addAttribute("studentProfile", studentProfile);
            return "studentProfile";
        } else return "errorStudentProfile";
    }
}
