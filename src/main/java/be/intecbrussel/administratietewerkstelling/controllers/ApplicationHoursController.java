package be.intecbrussel.administratietewerkstelling.controllers;

import be.intecbrussel.administratietewerkstelling.model.studentDocumentFiles.ApplicationHours;
import be.intecbrussel.administratietewerkstelling.services.ApplicationHoursServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;

import java.util.Optional;

@Controller
public class ApplicationHoursController {

    //TODO : discuss: are we sure we shouldn't put interface here for autowiring??
    @Autowired
    ApplicationHoursServiceImpl applicationHoursInterface;

    @PutMapping("/application")
    public String updateApplicationHours(@ModelAttribute ApplicationHours applicationHours) {
        applicationHoursInterface.updateApplicationHours(applicationHours);
        return "redirect:/student/" + applicationHours.getStudent().getId();

    }

    @GetMapping("/sollicitatiesDocument/{id}")
    public String getApplicationHours(@PathVariable int id, Model model) {
        Optional<ApplicationHours> applicationHours = applicationHoursInterface.getApplicationHoursById(id);
        if (applicationHours.isPresent()) {
            model.addAttribute("applicationHours", applicationHours);
            return "applicationHours";
        } else return "errorApplicationHours";
    }
}
