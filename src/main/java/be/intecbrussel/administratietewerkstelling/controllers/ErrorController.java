package be.intecbrussel.administratietewerkstelling.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ErrorController {

    @GetMapping("/error")
    public String error() {
        return "error1"; //html page with info on this specific error
    }

    @GetMapping("/errorAftercare")
    public String errorAftercare() {
        return "errorAftercare"; //html page with info on this specific error
    }
}
