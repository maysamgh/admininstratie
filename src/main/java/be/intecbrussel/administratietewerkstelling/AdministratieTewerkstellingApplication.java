package be.intecbrussel.administratietewerkstelling;

import be.intecbrussel.administratietewerkstelling.model.Training;
import be.intecbrussel.administratietewerkstelling.repositories.TrainingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import java.time.LocalDate;

@SpringBootApplication
public class AdministratieTewerkstellingApplication implements CommandLineRunner {

    @Autowired
    TrainingRepository repository;

    public static void main(String[] args) {
        ApplicationContext ctx = SpringApplication.run(AdministratieTewerkstellingApplication.class, args);
//       TrainingRepository repository = ctx.getBean(TrainingRepository.class);
//       Training training = new Training();
//       training.setName("test");
//       repository.save(training);
    }

    @Override
    public void run(String... args) throws Exception {
        Training training = new Training();
        training.setName("Opleiding1");
        training.setStartDate(LocalDate.of(2015, 12, 31));
        training.setEndDate(LocalDate.of(2018, 12, 31));
        repository.save(training);

        Training training2 = new Training();
        training2.setName("Opleiding2");
        training2.setStartDate(LocalDate.of(2015, 12, 31));
        training2.setEndDate(LocalDate.of(2018, 12, 31));
        repository.save(training2);

        Training training3 = new Training();
        training3.setName("Opleiding3");
        training3.setStartDate(LocalDate.of(2015, 12, 31));
        training3.setEndDate(LocalDate.of(2018, 12, 31));
        repository.save(training3);
//
//        Training training4 = new Training();
//        training.setName("Opleiding4");
//        training.setStartDate(LocalDate.of(2015, 12, 31));
//        training.setEndDate(LocalDate.of(2018, 12, 31));
//        repository.save(training4);

        //wat gebeurt er als we fouten ingeven voor start en eind datum??
//        Training training5 = new Training();
//        training.setName("Opleiding5");
//        training.setStartDate(LocalDate.of(2019, 12, 31));
//        training.setEndDate(LocalDate.of(2018, 12, 31));
//        repository.save(training5);
//
//        Training training6 = new Training();
//        training.setName("Opleiding1");
//        training.setStartDate(LocalDate.of(2018, 12, 31));
//        training.setEndDate(LocalDate.of(2018, 12, 31));
//        repository.save(training6);
    }
}
