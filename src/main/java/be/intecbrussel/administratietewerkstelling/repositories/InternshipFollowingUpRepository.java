package be.intecbrussel.administratietewerkstelling.repositories;

import be.intecbrussel.administratietewerkstelling.model.studentDocumentFiles.InternshipFollowingUp;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InternshipFollowingUpRepository extends JpaRepository<InternshipFollowingUp, Integer> {
}
