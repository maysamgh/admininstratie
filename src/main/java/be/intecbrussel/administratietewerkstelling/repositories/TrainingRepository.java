package be.intecbrussel.administratietewerkstelling.repositories;


import be.intecbrussel.administratietewerkstelling.model.Training;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TrainingRepository extends JpaRepository<Training, Integer> {

    List<Training> findAll();

    Training findTrainingById(int id);
}
