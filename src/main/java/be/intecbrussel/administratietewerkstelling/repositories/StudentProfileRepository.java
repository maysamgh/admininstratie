package be.intecbrussel.administratietewerkstelling.repositories;

import be.intecbrussel.administratietewerkstelling.model.studentDocumentFiles.StudentProfile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentProfileRepository extends JpaRepository<StudentProfile, Integer> {
}
