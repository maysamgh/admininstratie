package be.intecbrussel.administratietewerkstelling.repositories;

import be.intecbrussel.administratietewerkstelling.model.studentDocumentFiles.Aftercare;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AftercareRepository extends JpaRepository<Aftercare, Integer> {
}
